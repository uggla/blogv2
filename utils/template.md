+++
title = "$NAME"
date = "$DATE"
draft = false
template  = 'post.html'

[taxonomies]
categories = ["$CATEGORIES"]
tags = ["$TAGS"]

[extra]
lang = "fr"
toc = true
math = true
mermaid = true
biscuit = false
cc_license = true
outdate_warn = true
outdate_warn_days = 120

metas = [
    { name = "twitter:card", content="summary_large_image" },
    { name = "twitter:title", content="$TITLE" },
    { name = "twitter:image", content="https://lafor.ge/assets/thumbails/$THUMBNAIL.png" },
    { property = "og:type", content="website" },
    { property = "og:title", content="$TITLE" },
    { property = "og:image", content="https://lafor.ge/assets/thumbails/$THUMBNAIL.png" },
    { property = "og:url", content="https://lafor.ge/$URL" },
    { property = "og:image:width", content="1200" },
    { property = "og:image:heigth", content="675" },
]

+++
${INTRO}